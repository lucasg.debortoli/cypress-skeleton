import '../../support/commands.js';

describe('SauceDemo test', () => {

  beforeEach(() => {
    cy.visit('https://www.saucedemo.com/');
  });

  it('Login happy path', () => {
    cy.login('standard_user', 'secret_sauce');
    cy.url().should('eq', 'https://www.saucedemo.com/inventory.html');
  });

  it('Login wrong password should show an error message', () => {
    cy.login('standard_user', 'invalid password');
    cy.get('[data-test="error"]').should(
        'have.text',
        'Epic sadface: Username and password do not match any user in this service'
    );
  });

  it('Login wrong username should show an error message', () => {
    cy.login('invalid_user', 'secret_sauce');
    cy.get('[data-test="error"]').should(
        'have.text',
        'Epic sadface: Username and password do not match any user in this service'
    );
  });
});

