import '../../support/commands.js';

describe('SauceDemo test', () => {

  beforeEach(() => {
    cy.visit('https://www.saucedemo.com/');
    cy.login('standard_user', 'secret_sauce');
  });

  it.only('Checkout flow is working correctly', () => {
    cy.get('[data-test="add-to-cart-sauce-labs-backpack"]').click();
    cy.get('.shopping_cart_link').click();
    cy.get('[data-test="checkout"]').click();
    cy.get('[data-test="firstName"]').type('Lucas');
    cy.get('[data-test="lastName"]').type('De Bortoli');
    cy.get('[data-test="postalCode"]').type('1234567');
    cy.get('[data-test="continue"]').click();
    cy.get('[data-test="finish"]').click();
    cy.get('.complete-header').should('have.text', 'Thank you for your order!');
    cy.get('.complete-text').should(
        'have.text',
        'Your order has been dispatched, and will arrive just as fast as the pony can get there!'
    );
  });
});

